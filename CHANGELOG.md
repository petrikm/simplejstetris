
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2021-08-19
### Added
 *  README and CHANGELOG
### Changed
 *  JavaScript source code sorted to modules.
 *  CSS moved to separate file.
 *  Collision checking is now handled by the class Brick (and not by main.js).
 *  Removed checking of upper boundary: the brick now can be rotated right
    after it appears on the game panel. Further, the new brick appears now one
    box higher, partly hidden behind the upper boundary.
### Removed
 *  Clickable buttons

## [0.3.0] - 2021-08-12
### Added
 *  Clickable buttons
### Changed
 *  HTML layout of the page
 *  direction of rotation
 *  "T"-brick rotation pivot

## [0.2.0] - 2021-08-11
### Added
 *  Control of the falling brick by keyboard.
 *  Placing the falling brick.
 *  Pausing and restarting of the game.
 *  Deleting full rows while counting the score:
    - 1 for one row,
    - 1+2=3 for two rows,
    - 1+2+3=6 for three rows
    - 1+2+3+4=10 for four rows.
 *  Displaying the score.
### Changed
 *  Collision checking rewritten thoroughly.

## [0.1.0] - 2021-08-05
 *  First running prototype

[0.4.0]: https://gitlab.com/petrikm/simplejstetris/-/tags/0.4.0
[0.3.0]: https://gitlab.com/petrikm/simplejstetris/-/tags/0.3.0
[0.2.0]: https://gitlab.com/petrikm/simplejstetris/-/tags/0.2.0
[0.1.0]: https://gitlab.com/petrikm/simplejstetris/-/tags/0.1.0
