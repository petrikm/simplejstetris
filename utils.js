/*
 * This file is a part of simplejstetris which is a JavaScript program which
 * implements the game Tetris.
 *
 * MIT License
 *
 * Copyright (c) 2021 Milan Petrík <milan.petrik at protonmail.com>
 *
 * Web page of the program: <https://gitlab.com/petrikm/simplejstetris>
 */

/*
 * This file contains some utility function which deal with drawing and erasing
 * the bricks on the game panel.
 */

import * as constants from "./constants.js";

/*
 * Fills one box of the HTML table that implements the game panel
 * by the given color.
 * The color is supposed to be specified by the CSS syntax.
 */
export function drawTetrisCell(x, y, color) {
  document.getElementById('tetris cell ' + x + ' ' + y).style = 'background-color: ' + color;
}

/*
 * Erases one box of the HTML table that implements the game panel.
 * That is, it is filled by no color.
 */
export function eraseTetrisCell(x, y) {
  document.getElementById('tetris cell ' + x + ' ' + y).style = '';
}

/*
 * Fills one box of the HTML table that implements the panel to draw the next
 * brick.
 * The color is supposed to be specified by the CSS syntax.
 */
export function drawNextBrickCell(x, y, color) {
  document.getElementById('next brick cell ' + x + ' ' + y).style = 'background-color: ' + color;
}

/*
 * Erases one box of the HTML table that implements the panel to draw the next
 * brick.
 * That is, it is filled by no color.
 */
export function eraseNextBrickCell(x, y) {
  document.getElementById('next brick cell ' + x + ' ' + y).style = '';
}

/*
 * Erases the whole panel that displays the next brick.
 */
export function eraseNextBrick(x, y) {
  for(let y = 0; y < constants.NUM_NEXT_BRICK_ROWS; y++) {
    for(let x = 0; x < constants.NUM_NEXT_BRICK_COLS; x++) {
      eraseNextBrickCell(x, y);
    }
  }
}
