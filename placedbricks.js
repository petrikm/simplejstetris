/*
 * This file is a part of simplejstetris which is a JavaScript program which
 * implements the game Tetris.
 *
 * MIT License
 *
 * Copyright (c) 2021 Milan Petrík <milan.petrik at protonmail.com>
 *
 * Web page of the program: <https://gitlab.com/petrikm/simplejstetris>
 */

import * as constants from "./constants.js";
import * as utils from "./utils.js";

/*
 * class PlacedBricks
 *
 * Represents the (parts of the) bricks that have been already placed to the
 * game panel and are immovable.
 */
class PlacedBricks {
  /*
   * Creates a 2d array of the same size as is the game panel.
   * The value of each item of the array then determines whether the
   * corresponding box of the game panel is occupied by a placed brick, or not.
   */
  constructor() {
    this.arr = new Array(constants.NUM_COLS);
    for (let x = 0; x < constants.NUM_COLS; ++x) {
      this.arr[x] = new Array(constants.NUM_ROWS).fill(constants.BRICK_NONE);
    }
  }

  /*
   * Adds a brick (an instance of Brick) to the placed bricks.
   * This happens when the falling brick lands either on the bottom of the game
   * panel or on already placed bricks.
   */
  place(brick) {
    for (let box of brick.boxes) {
      let x = brick.x + box[0];
      let y = brick.y + box[1];
      this.arr[x][y] = brick.type;
    }
  }

  /*
   * Checks whether the brick (specified by the argument) is in a collision
   * with the placed bricks.
   */
  isCollision(brick) {
    for (let box of brick.boxes) {
      let x = brick.x + box[0];
      let y = brick.y + box[1];
      if (x >= 0 && x < constants.NUM_COLS && y >= 0 && y < constants.NUM_ROWS
          && this.arr[x][y] != constants.BRICK_NONE) {
        return true;
      }
    }
    return false;
  }

  /*
   * Checks whether there are full rows.
   * Returns:
   *    array of the indices (y-coordinates) of the full rows,
   *    or an empty array of there is no full row
   */
  detectFullRows() {
    let fullRows = [];
    for (let y = 0; y < constants.NUM_ROWS; ++y) {
      let isFullRow = true;
      for (let x = 0; x < constants.NUM_COLS; ++x) {
        if (this.arr[x][y] == constants.BRICK_NONE) {
          isFullRow = false;
          break;
        }
      }
      if (isFullRow) {
        fullRows.push(y);
      }
    }
    return fullRows;
  }

  /*
   * Draws the placed bricks to the game panel.
   *
   * Args:
   *    gameOver (boolean): indicates whether the game is over, or not;
   *        depending on this value, the bricks are drawn in color (if the game
   *        is running) or in gray (if the game is over)
   */
  draw(gameOver) {
    for (let y = 0; y < constants.NUM_ROWS; ++y) {
      for (let x = 0; x < constants.NUM_COLS; ++x) {
        if (this.arr[x][y] == constants.BRICK_NONE) {
          utils.eraseTetrisCell(x, y);
        } else {
          if (gameOver) {
            utils.drawTetrisCell(x, y, constants.GAME_OVER_BRICK_COLOR);
          } else {
            utils.drawTetrisCell(x, y, constants.BRICK_COLOR[this.arr[x][y]]);
          }
        }
      }
    }
  }
}

export default PlacedBricks;
