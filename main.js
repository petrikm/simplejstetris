/*
 * This file is a part of simplejstetris which is a JavaScript program which
 * implements the game Tetris.
 *
 * MIT License
 *
 * Copyright (c) 2021 Milan Petrík <milan.petrik at protonmail.com>
 *
 * Web page of the program: <https://gitlab.com/petrikm/simplejstetris>
 */

import * as constants from "./constants.js";
import * as utils from "./utils.js";
import Brick from "./brick.js";
import PlacedBricks from "./placedbricks.js";

// Global variables
var placedBricks = new PlacedBricks();  // bricks that has been already placed
var brick = null;             // current falling brick
var nextBrick = new Brick(placedBricks);  // brick that replaces the current brick after it is placed
var score = 0;
var gameOver = false; // indicates whether the game is lost
var paused = false;   // indicates whether the game is paused
var loopDelay = 750;  // time between two game steps (frames) in miliseconds

// Global variables related to removing of full rows
var remDelay = 100;
var remFullRows = [];
var remX = 0;
var remY = 0;
var remDiff = 0;
var remInterval;

/*
 * Rotates the falling brick.
 * Implements both the clickable button and the keyboard key "up".
 */
function rotateBrick() {
  if (brick != null) {
    if (brick.rotate()) {
      placedBricks.draw(gameOver);
      brick.draw();
    }
  }
}

/*
 * Moves the falling brick by one box to the left.
 * Implements both the clickable button and the keyboard key "left".
 */
function moveBrickLeft() {
  if (brick != null) {
    if (brick.moveLeft()) {
      placedBricks.draw(gameOver);
      brick.draw();
    }
  }
}

/*
 * Moves the falling brick by one box to the right.
 * Implements both the clickable button and the keyboard key "right".
 */
function moveBrickRight() {
  if (brick != null) {
    if (brick.moveRight()) {
      placedBricks.draw(gameOver);
      brick.draw();
    }
  }
}

/*
 * Moves the falling brick by one box down.
 * Implements both the clickable button and the keyboard key "down".
 */
function moveBrickDown() {
  if (brick != null) {
    if (!brick.moveDown()) {
      placedBricks.place(brick);
      detectAndRemoveFullRows();
      brick = null;
    }
    placedBricks.draw(gameOver);
    if (!gameOver && brick != null) {
      brick.draw();
    }
  }
}

/*
 * Pushes the falling brick down.
 * The brick is moved down until a collision is detected.
 * Then the brick is "placed" and a new falling brick is created.
 * This function implements both the clickable button and the keyboard
 * "spacebar".
 */
function pushBrickDown() {
  if (brick != null) {
    brick.pushDown();
    placedBricks.place(brick);
    detectAndRemoveFullRows();
    brick = null;
    placedBricks.draw(gameOver);
    if (!gameOver && brick != null) {
      brick.draw();
    }
  }
}

/*
 * Pauses the game.
 * If the game is running then it is "paused" (it stops running and "Game
 * pause" appears onthe top of the page).
 * If the game is "paused" then it is "unpaused" (it continues running).
 */
function pauseGame() {
  if (paused) {
    paused = false;
    loopInterval = setInterval(loop, loopDelay);
    document.getElementById("header").innerHTML = constants.TITLE;
  } else {
    paused = true;
    clearInterval(loopInterval);
    document.getElementById("header").innerHTML = "*** GAME PAUSED ***";
  }
}

/*
 * Restarts the game.
 * All the placed bricks are removed, the score is reset, and the game starts
 * from scratch.
 */
function restartGame() {
  placedBricks = new PlacedBricks();
  brick = null;
  nextBrick = new Brick(placedBricks);
  score = 0;
  document.getElementById("score").innerHTML = score;
  gameOver = false;
  paused = false;
  placedBricks.draw(gameOver);
}

/*
 * Detects full rows (rows in game panel that are fully filled by placed
 * bricks) and runs the loop to remove them.
 */
function detectAndRemoveFullRows() {
  remFullRows = placedBricks.detectFullRows();
  if (remFullRows.length > 0) {
    clearInterval(loopInterval);
    remDiff = remFullRows.length;
    remX = 0;
    remY = remFullRows.shift();
    remInterval = setInterval(removingFullRows, remDelay);
  }
}

/*
 * Loop to perform the animation when removing full rows.
 */
function removingFullRows() {
  utils.eraseTetrisCell(remX, remY);
  ++remX;
  if (remX >= constants.NUM_COLS) {
    if (remFullRows.length > 0) {
      remX = 0;
      remY = remFullRows.shift();
    } else {
      for (let y = remY; y >= remDiff; --y) {
        for (let x = 0; x < constants.NUM_COLS; ++x) {
          placedBricks.arr[x][y] = placedBricks.arr[x][y - remDiff];
        }
      }
      for (let y = 0; y < remDiff; ++y) {
        for (let x = 0; x < constants.NUM_COLS; ++x) {
          placedBricks.arr[x][y] = constants.BRICK_NONE;
        }
      }
      clearInterval(remInterval);
      loopInterval = setInterval(loop, loopDelay);
      placedBricks.draw();
      score += Math.round(remDiff * (remDiff + 1) / 2);
      document.getElementById("score").innerHTML = score;
    }
  }
}

/*
 * Main loop
 */
function loop() {
  if (!gameOver) {
    if (brick == null) {
      brick = nextBrick;
      nextBrick = new Brick(placedBricks);
      utils.eraseNextBrick();
      if (placedBricks.isCollision(brick)) {
        gameOver = true;
      }
    } else {
      if (!brick.moveDown()) {
        placedBricks.place(brick);
        detectAndRemoveFullRows();
        brick = null;
      }
    }
  }
  placedBricks.draw(gameOver);
  if (!gameOver) {
    if (brick != null) {
      brick.draw();
    }
    if (nextBrick != null) {
      nextBrick.drawAsNextBrick();
    }
  }
}

document.querySelector("title").innerHTML = constants.TITLE;
document.getElementById("header").innerHTML = constants.TITLE;
document.getElementById("score").innerHTML = score;

// Construction of the game panel
var tetrisTableContent = '';
for(let y = 0; y < constants.NUM_ROWS; y++) {
  tetrisTableContent += '<tr id="tetris row ' + y + '" class="tetris">';
  for(let x = 0; x < constants.NUM_COLS; x++) {
    tetrisTableContent += '<td id="tetris cell ' + x + ' ' + y + '" class="tetris">';
    tetrisTableContent += '&nbsp;';
    tetrisTableContent += '</td>';
  }
  tetrisTableContent += '</tr>';
}
document.getElementById("tetris").innerHTML = tetrisTableContent;

// Construction of the panel to draw the next brick
var nextBrickTableContent = '';
for(let y = 0; y < constants.NUM_NEXT_BRICK_ROWS; y++) {
  nextBrickTableContent += '<tr id="next brick row ' + y + '" class="tetris">';
  for(let x = 0; x < constants.NUM_NEXT_BRICK_COLS; x++) {
    nextBrickTableContent += '<td id="next brick cell ' + x + ' ' + y + '" class="tetris">';
    nextBrickTableContent += '&nbsp;';
    nextBrickTableContent += '</td>';
  }
  nextBrickTableContent += '</tr>';
}
document.getElementById("next brick").innerHTML = nextBrickTableContent;

// Make buttons to change color when under mouse cursor.
// This would be better solved by CSS but I just wanted to do it in JavaScript
// for learning purposes.
document.querySelectorAll("button").forEach(
  item => {
    item.addEventListener("mouseover", function(event) {
      this.style.backgroundColor='LightSkyBlue';
    })
    item.addEventListener("mouseout", function(event) {
      this.style.backgroundColor='DodgerBlue';
    })
  }
);

// Handling of the keyboard input
document.addEventListener("keydown", function(event) {
  let kc = event.keyCode;
  if (!gameOver) {
    switch(kc) {
      case 37: // left
        moveBrickLeft();
        break;
      case 39: // right
        moveBrickRight();
        break;
      case 38: // up ... rotate the brick
        rotateBrick();
        break;
      case 40: // down
        moveBrickDown();
        break;
      case 32: // space
        pushBrickDown();
        break;
      case 80: // P ... pause the game
        pauseGame();
        break;
    }
  }
  switch(kc) {
    case 82: // R ... restart the game
      restartGame();
      break;
  }
});

// Draw the game panel
placedBricks.draw();

// Start the main loop
var loopInterval = setInterval(loop, loopDelay);

