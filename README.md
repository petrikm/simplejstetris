Simple JavaScript Tetris
========================

This is the most basic variant of the game Tetris written in JavaScript.

The main purpose for me to write this program was to learn this programming
language.

You can see how it runs on
[http://176.102.65.176/~milan/simplejstetris/](http://176.102.65.176/~milan/simplejstetris/).
