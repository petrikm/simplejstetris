/*
 * This file is a part of simplejstetris which is a JavaScript program which
 * implements the game Tetris.
 *
 * MIT License
 *
 * Copyright (c) 2021 Milan Petrík <milan.petrik at protonmail.com>
 *
 * Web page of the program: <https://gitlab.com/petrikm/simplejstetris>
 */

/*
 * This file contains the global constants that are used by the program.
 */

// The title of the program;
// it is displayed in the header of the HTML page,
// it is the value of the <title> tag
export const TITLE = "Simple JavaScript Tetris";

// Size of the game panel
export const NUM_ROWS = 20;
export const NUM_COLS = 10;

// Size of the panel with "Next Brick"
export const NUM_NEXT_BRICK_ROWS = 2;
export const NUM_NEXT_BRICK_COLS = 4;

// Brick types
export const BRICK_NONE = -1;
export const BRICK_LINE = 0;
export const BRICK_T = 1;
export const BRICK_RIGHT_STEP = 2;
export const BRICK_LEFT_STEP = 3;
export const BRICK_SQUARE = 4;
export const BRICK_L = 5;
export const BRICK_J = 6;

// For converting between constant names and their numeric representations
export const BRICK_TYPE = [
  'BRICK_LINE',
  'BRICK_T',
  'BRICK_RIGHT_STEP',
  'BRICK_LEFT_STEP',
  'BRICK_SQUARE',
  'BRICK_L',
  'BRICK_J'];

// Brick type colors
export const BRICK_COLOR = [
  'Tomato',
  'Orange',
  'DodgerBlue',
  'MediumSeaGreen',
  'Gray',
  'SlateBlue',
  'Violet'];

// When the game is over, the placed bricks are recolored to this color
export const GAME_OVER_BRICK_COLOR = 'LightGray';
