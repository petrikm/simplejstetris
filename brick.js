/*
 * This file is a part of simplejstetris which is a JavaScript program which
 * implements the game Tetris.
 *
 * MIT License
 *
 * Copyright (c) 2021 Milan Petrík <milan.petrik at protonmail.com>
 *
 * Web page of the program: <https://gitlab.com/petrikm/simplejstetris>
 */

import * as constants from "./constants.js";
import * as utils from "./utils.js";

/*
 * class Brick
 *
 * Represents one Tetris brick which is formed out of four boxes of the game
 * panel.
 */
class Brick {
  /*
   * Creates a brick at the center-top position of the game panel.
   * The type of the brick is randomly chosen from the possible brick types
   * (see constants.js).
   *
   * Args:
   *    placedBricks (PlacedBricks): serves to detect collisions with already
   *        placed bricks
   */
  constructor(placedBricks) {
    this.placedBricks = placedBricks;
    // determines the type of the brick
    this.type = Math.floor(constants.BRICK_COLOR.length * Math.random());
    // x coordinate; 0 is the very left one
    this.x = Math.floor(constants.NUM_COLS / 2) - 1;
    // y coordinate; 0 is the top one
    this.y = -1;
    // coordinates of the boxes (squares) that form the brick
    switch(this.type) {
      // XXXX
      case constants.BRICK_LINE:
        this.boxes = [ [-1,0], [0,0], [1,0], [2,0] ];
        break;
      // XXX
      //  X
      case constants.BRICK_T:
        this.boxes = [ [0,1], [-1,0], [0,0], [1,0] ];
        break;
      //  XX
      // XX
      case constants.BRICK_RIGHT_STEP:
        this.boxes = [ [0,0], [1,0], [-1,1], [0,1] ];
        break;
      // XX
      //  XX
      case constants.BRICK_LEFT_STEP:
        this.boxes = [ [-1,0], [0,0], [0,1], [1,1] ];
        break;
      // XX
      // XX
      case constants.BRICK_SQUARE:
        this.boxes = [ [0,0], [1,0], [0,1], [1,1] ];
        break;
      // XXX
      // X
      case constants.BRICK_L:
        this.boxes = [ [-1,0], [0,0], [1,0], [-1,1] ];
        break;
      // XXX
      //   X
      case constants.BRICK_J:
      default:
        this.type = constants.BRICK_J;
        this.boxes = [ [-1,0], [0,0], [1,0], [1,1] ];
        break;
    }
    this.color = constants.BRICK_COLOR[this.type];
  }

  /*
   * Moves the brick by one box to the left.
   *
   * If there is a collision with the boundaries of the game panel or with the
   * placed bricks, the brick is immediately placed by one box back to the
   * right.
   *
   * Returns (boolean):
   *    `true` if the brick has been shifted to the left without a collision,
   *    `false` if there was collision and the brick has not been shifted left
   */
  moveLeft() {
    // move brick to the left
    --this.x;

    // check whether the brick went out of boundaries
    let outOfBoundaries = false;
    for (let box of this.boxes) {
      let x = this.x + box[0]; // x-coordinate of the box
      if (x < 0) {
        outOfBoundaries = true;
        break;
      }
    }

    if (outOfBoundaries || this.placedBricks.isCollision(this)) {
      // if there is a collision, move brick back to the right
      ++this.x;
      return false;
    } else {
      return true;
    }
  }

  /*
   * Moves the brick by one box to the right.
   *
   * If there is a collision with the boundaries of the game panel or with the
   * placed bricks, the brick is immediately placed by one box back to the
   * left.
   *
   * Returns (boolean):
   *    `true` if the brick has been shifted to the right without a collision,
   *    `false` if there was collision and the brick has not been shifted right
   */
  moveRight() {
    // move brick to the right
    ++this.x;

    // check whether the brick went out of boundaries
    let outOfBoundaries = false;
    for (let box of this.boxes) {
      let x = this.x + box[0]; // x-coordinate of the box
      if (x >= constants.NUM_COLS) {
        outOfBoundaries = true;
        break;
      }
    }

    if (outOfBoundaries || this.placedBricks.isCollision(this)) {
      // if there is a collision, move brick back to the left
      --this.x;
      return false;
    } else {
      return true;
    }
  }

  /* 
   * Moves the brick by one box down.
   *
   * If there is a collision with the boundaries of the game panel or with the
   * placed bricks, the brick is immediately placed by one box up.
   *
   * Returns (boolean):
   *    `true` if the brick has been shifted down without a collision,
   *    `false` if there was collision and the brick has not been shifted down
   */
  moveDown() {
    // move brick by one box down
    ++this.y;

    // check whether the brick went out of boundaries
    let outOfBoundaries = false;
    for (let box of this.boxes) {
      let y = this.y + box[1]; // y-coordinate of the box
      if (y >= constants.NUM_ROWS) {
        outOfBoundaries = true;
        break;
      }
    }

    if (outOfBoundaries || this.placedBricks.isCollision(this)) {
      // if there is a collision, move brick back up
      --this.y;
      return false;
    } else {
      return true;
    }
  }

  /* 
   * Pushes the brick by one box down, that is, moves the brick as down as
   * possible without a collision.
   */
  pushDown() {
    while (this.moveDown()) {}
  }

  /* 
   * Rotates the brick by 90 degress clockwise.
   *
   * If there is a collision with the boundaries of the game panel or with the
   * placed bricks, the brick is immediately rotated back by 90 degress
   * counter-clockwise.
   *
   * Returns (boolean):
   *    `true` if the brick has been rotated without a collision,
   *    `false` if there was collision and the brick has not been rotated
   */
  rotate() {
    if (this.type === constants.BRICK_SQUARE) {
      // BRICK_SQUARE can always be rotated
      return true;
    } else {
      // rotate the brick by 90 degress clockwise
      for (let box of this.boxes) {
        var x = box[1];
        var y = -box[0];
        box[0] = x;
        box[1] = y;
      }

      // check whether the brick went out of boundaries
      let outOfBoundaries = false;
      for (let box of this.boxes) {
        let x = this.x + box[0]; // x-coordinate of the box
        let y = this.y + box[1]; // y-coordinate of the box
        // the upeer boundary is not checked
        if (x < 0 || x >= constants.NUM_COLS || y >= constants.NUM_ROWS) {
          outOfBoundaries = true;
          break;
        }
      }

      if (outOfBoundaries || this.placedBricks.isCollision(this)) {
        // if there is a collision, rotate the brick back
        for (let box of this.boxes) {
          var x = -box[1];
          var y = box[0];
          box[0] = x;
          box[1] = y;
        }
        return false;
      } else {
        return true;
      }
    }
  }

  /*
   * Draws the brick to the game panel.
   * That is, the corresponding boxes of the HTML table that implements
   * the game panel are filled by the color of the brick.
   */
  draw() {
    for (let box of this.boxes) {
      let x = this.x + box[0];
      let y = this.y + box[1];
      if (x >= 0 && x < constants.NUM_COLS && y >= 0 && y < constants.NUM_ROWS) {
        utils.drawTetrisCell(x, y, this.color);
      }
    }
  }

  /*
   * Draws the brick to the panel that displays the next brick.
   */
  drawAsNextBrick() {
    for (let box of this.boxes) {
      let x = box[0] + 1;
      let y = box[1];
      utils.drawNextBrickCell(x, y, this.color);
    }
  }

  /*
   * Erases the brick from the game panel.
   * That is, the corresponding boxes of the HTML table that implements
   * the game panel are filled by no color.
   */
  erase() {
    for (let box of this.boxes) {
      let x = this.x + box[0];
      let y = this.y + box[1];
      eraseTetrisCell(x, y);
    }
  }
}

export default Brick;
